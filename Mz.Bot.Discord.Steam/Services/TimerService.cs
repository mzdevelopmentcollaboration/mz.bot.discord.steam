﻿using Serilog;
using Microsoft.Extensions.Configuration;

using Mz.Bot.Discord.Steam.Managers;

namespace Mz.Bot.Discord.Steam.Services;

internal class TimerService
{
    private readonly ILogger _logger;
    private readonly ParsingManager _parsingManager;
    private readonly BrowserService _browserService;
    private readonly DatabaseService _databaseService;
    private readonly ReportingService _reportingService;

    public TimerService(BrowserService browserService, IConfigurationRoot configuration,
        DatabaseService databaseService, ReportingService reportingService)
    {
        _logger = Log.Logger;
        _browserService = browserService;
        _databaseService = databaseService;
        _reportingService = reportingService;
        _parsingManager = new ParsingManager(configuration);
    }

    public async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        while (!cancellationToken.IsCancellationRequested)
        {
            var steamWorkshopContent = await _browserService.FetchSteamWorkshopContentAsync();
            if (steamWorkshopContent != string.Empty)
            {
                var steamWorkshopItems = _parsingManager.ParseSteamWorkshopContent(steamWorkshopContent);
                var newSteamWorkshopItems =
                    await _databaseService.FindNonExistingSteamWorkshopItemsAsync(steamWorkshopItems);
                if (newSteamWorkshopItems.Any())
                {
                    await _databaseService.InsertSteamWorkshopItemsAsync(newSteamWorkshopItems);
                    var reportingTasks = newSteamWorkshopItems.Select(newSteamWorkshopItem =>
                        _reportingService.PostWorkshopItemAsync(newSteamWorkshopItem)).ToArray();
                    Task.WaitAll(reportingTasks);
                    _logger.Information($"Reported on {newSteamWorkshopItems.Count} new item/s.");
                }
                else
                {
                    _logger.Information("Successfully checked for new items, none were present.");
                }
            }
            else
            {
                _logger.Error("Could not fetch the steamWorkshopContent. Is something wrong with the browser?");
            }
            await Task.Delay(TimeSpan.FromMinutes(30), cancellationToken);
        }
    }
}
