﻿using Microsoft.Extensions.Configuration;

using PuppeteerSharp;

namespace Mz.Bot.Discord.Steam.Services;

internal class BrowserService
{
    private Browser _browser;
    private readonly string _steamWorkshopUrl;
    private readonly ResourceType[] _skippedResourceTypes = {
        ResourceType.Image,
        ResourceType.Img,
        ResourceType.Media,
        ResourceType.StyleSheet
    };

    public BrowserService(IConfiguration configuration)
    {
        _browser = Task.Run(CreateBrowserAsync).Result;
        _steamWorkshopUrl = configuration.GetSection("SteamWorkshop:Url").Get<string>();
    }

    public async Task<string> FetchSteamWorkshopContentAsync()
    {
        var body = string.Empty;
        for (var i = 1; i <= 3; i++)
        {
            try
            {
                var page = await GenerateModifiedPageAsync();
                await page.SetRequestInterceptionAsync(true);
                page.Request += PageOnRequestAsync;
                await page.GoToAsync(_steamWorkshopUrl);
                body = await page.GetContentAsync();
                await page.DisposeAsync();
                return body;
            }
            catch (Exception)
            {
                if (!_browser.IsConnected)
                {
                    await _browser.DisposeAsync();
                    _browser = await CreateBrowserAsync();
                }
            }
        }
        return body;
    }

    // ReSharper disable once MemberCanBeMadeStatic.Local
    private async Task<Browser> CreateBrowserAsync()
    {
        var connectOptions = new ConnectOptions
        {
            BrowserWSEndpoint = Environment.GetEnvironmentVariable("BROWSER_WS_ENDPOINT")
        };
        var browser = await Puppeteer.ConnectAsync(connectOptions);
        return browser;
    }

    private async Task<Page> GenerateModifiedPageAsync()
    {
        var page = await _browser.NewPageAsync();
        var random = new Random();
        var randomInteger = Convert.ToInt32(random.Next(0, Puppeteer.Devices.Count));
        await page.EmulateAsync(Puppeteer.Devices.ElementAt(randomInteger).Value);
        return page;
    }

    private async void PageOnRequestAsync(object? sender, RequestEventArgs requestEventArguments)
    {
        if (_skippedResourceTypes.Contains(requestEventArguments.Request.ResourceType))
        {
            await requestEventArguments.Request.AbortAsync();
            return;
        }
        await requestEventArguments.Request.ContinueAsync();
    }
}
