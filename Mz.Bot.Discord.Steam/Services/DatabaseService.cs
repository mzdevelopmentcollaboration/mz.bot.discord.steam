﻿using Microsoft.Extensions.Configuration;

using MongoDB.Driver;

using Mz.Bot.Discord.Steam.Models;

namespace Mz.Bot.Discord.Steam.Services;

internal class DatabaseService
{
    private readonly IMongoCollection<SteamWorkshopItem> _steamWorkshopItems;

    public DatabaseService(IConfigurationRoot configuration)
    {
        var databaseClient = new MongoClient(Environment.GetEnvironmentVariable("DATABASE_CONNECTION_STRING"));
        var database = databaseClient.GetDatabase("mz-bot-discord-steam");
        _steamWorkshopItems = database.GetCollection<SteamWorkshopItem>("steam-workshop-items");
    }

    public async Task InsertSteamWorkshopItemsAsync(IEnumerable<SteamWorkshopItem> steamWorkshopItems) =>
        await _steamWorkshopItems.InsertManyAsync(steamWorkshopItems);

    public async Task<List<SteamWorkshopItem>> FindNonExistingSteamWorkshopItemsAsync(IEnumerable<SteamWorkshopItem> fetchedSteamWorkshopItems)
    {
        var newSteamWorkshopItems = new List<SteamWorkshopItem>();
        foreach (var fetchedSteamWorkshopItem in fetchedSteamWorkshopItems)
        {
            var steamWorkshopItemsCursor =
                await _steamWorkshopItems.FindAsync(x => x.Url == fetchedSteamWorkshopItem.Url);
            if (!await steamWorkshopItemsCursor.AnyAsync())
            {
                newSteamWorkshopItems.Add(fetchedSteamWorkshopItem);
            }
        }
        return newSteamWorkshopItems;
    }
}
