﻿using Discord.Webhook;
using Microsoft.Extensions.Configuration;

using Mz.Bot.Discord.Steam.Models;
using Mz.Bot.Discord.Steam.Managers;

namespace Mz.Bot.Discord.Steam.Services;

internal class ReportingService
{
    private readonly string _discordWebhookUrl;
    private readonly DiscordManager _discordManager;

    public ReportingService(IConfigurationRoot configuration)
    {
        _discordWebhookUrl = Environment.GetEnvironmentVariable("DISCORD_WEBHOOK_URL") ??
            throw new InvalidOperationException("DiscordWebhookUrl from ENV is empty");
        _discordManager = new DiscordManager();
    }

    public async Task PostWorkshopItemAsync(SteamWorkshopItem steamWorkshopItem)
    {
        var discordWebhookClient = new DiscordWebhookClient(_discordWebhookUrl);
        var discordMessageEmbed = _discordManager.GenerateSteamWorkshopItemEmbed(steamWorkshopItem);
        await discordWebhookClient.SendMessageAsync(embeds: new[] {discordMessageEmbed});
    }
}
