﻿using Microsoft.Extensions.Configuration;

using Serilog;
using HtmlAgilityPack;

using Mz.Bot.Discord.Steam.Models;

namespace Mz.Bot.Discord.Steam.Managers;

internal class ParsingManager
{
    private readonly ILogger _logger;
    private readonly IConfigurationRoot _configuration;
    private const string PlaceholderValue = "PLACEHOLDER";

    public ParsingManager(IConfigurationRoot configuration)
    {
        _logger = Log.Logger;
        _configuration = configuration;
    }

    public List<SteamWorkshopItem> ParseSteamWorkshopContent(string steamWorkshopContent)
    {
        var itemTitleXPath = _configuration.GetSection("SteamWorkshop:ItemTitleXPath").Get<string>();
        var itemAuthorXPath = _configuration.GetSection("SteamWorkshop:ItemAuthorXPath").Get<string>();
        var itemUrlXPath = _configuration.GetSection("SteamWorkshop:ItemUrlXPath").Get<string>();
        var itemImageUrlXPath = _configuration.GetSection("SteamWorkshop:ItemImageUrlXPath").Get<string>();
        var itemsPerPageCount = _configuration.GetSection("SteamWorkShop:ItemsPerPageCount").Get<int>();
        var steamWorkshopItems = new List<SteamWorkshopItem>();
        var htmlDocument = new HtmlDocument();
        htmlDocument.LoadHtml(steamWorkshopContent);
        var documentNode = htmlDocument.DocumentNode;
        for (var i = 1; i <= itemsPerPageCount; i++)
        {
            var modifiedItemTitlePath = itemTitleXPath.Replace(PlaceholderValue, i.ToString());
            var modifiedItemAuthorPath = itemAuthorXPath.Replace(PlaceholderValue, i.ToString());
            var modifiedItemUrlPath = itemUrlXPath.Replace(PlaceholderValue, i.ToString());
            var modifiedItemImageUrlPath = itemImageUrlXPath.Replace(PlaceholderValue, i.ToString());
            try
            {
                var steamWorkshopItem = new SteamWorkshopItem
                {
                    Title = documentNode.SelectSingleNode(modifiedItemTitlePath).InnerText,
                    Author = documentNode.SelectSingleNode(modifiedItemAuthorPath).InnerText,
                    Url = documentNode.SelectSingleNode(modifiedItemUrlPath).Attributes["href"].Value
                        .Replace("&amp;searchtext=", string.Empty),
                    ImageUrl = documentNode.SelectSingleNode(modifiedItemImageUrlPath).Attributes["src"].Value
                };
                steamWorkshopItems.Add(steamWorkshopItem);
            }
            catch (Exception genericException)
            {
                _logger.Error(genericException, $"There was an error while parsing [{documentNode.Id}]");
            }
        }
        return steamWorkshopItems;
    }
}
