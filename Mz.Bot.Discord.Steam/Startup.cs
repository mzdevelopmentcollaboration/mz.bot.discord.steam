﻿using Serilog;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Mz.Bot.Discord.Steam.Services;

namespace Mz.Bot.Discord.Steam;

internal class Startup
{
    private IConfigurationRoot Configuration { get; }

    public Startup()
    {
        var configurationBuilder = new ConfigurationBuilder()
            .SetBasePath(AppContext.BaseDirectory)
            .AddJsonFile("config.json", false);
        Configuration = configurationBuilder.Build();
    }

    public static async Task RunAsync(string[] args)
    {
        var startup = new Startup();
        await startup.RunAsync();
    }

    private async Task RunAsync()
    {
        SetupLogger();
        var services = new ServiceCollection();
        ConfigureServices(services);
        var serviceProvider = services.BuildServiceProvider();
        serviceProvider.GetRequiredService<DatabaseService>();
        serviceProvider.GetRequiredService<BrowserService>();
        //TODO: Do something useful with this Token
        var cancellationTokenSource = new CancellationTokenSource();
        await serviceProvider.GetRequiredService<TimerService>()
            .ExecuteAsync(cancellationTokenSource.Token);
    }

    private static void SetupLogger()
    {
        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Information()
            .WriteTo.Console()
            .CreateLogger();
    }

    private void ConfigureServices(IServiceCollection services)
    {
        services.AddSingleton<DatabaseService>()
            .AddSingleton(new BrowserService(Configuration))
            .AddSingleton<ReportingService>()
            .AddSingleton<TimerService>()
            .AddSingleton(Configuration);
    }
}
