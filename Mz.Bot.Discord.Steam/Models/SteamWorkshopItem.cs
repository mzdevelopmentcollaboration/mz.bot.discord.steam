﻿using MongoDB.Bson;

namespace Mz.Bot.Discord.Steam.Models;

internal class SteamWorkshopItem
{
    public SteamWorkshopItem()
    {
        Id = ObjectId.GenerateNewId();
        UtcAdditionDateTime = DateTime.UtcNow;
    }

    public ObjectId Id { get; }
    public string? Url { get; set; }
    public string? Title { get; set; }
    public string? Author { get; set; }
    public string? ImageUrl { get; set; }
    public DateTime UtcAdditionDateTime { get; }
}
